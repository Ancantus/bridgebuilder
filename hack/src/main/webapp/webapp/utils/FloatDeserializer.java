package webapp.utils;

import java.lang.reflect.Type;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;

public class FloatDeserializer implements JsonDeserializer<Float>
{
   public Float deserialize(JsonElement floatStr, Type typeOfSrc, JsonDeserializationContext context)
   {
      try
      {
    	  return Float.parseFloat(floatStr.getAsString());
      } 
      catch (NumberFormatException e)
      {
    	  return null;
      }
      
   }
}

