package webapp.api.career;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import webapp.api.ConnectionManager;

public class CareerDAO extends ConnectionManager{
	
	public CareerDAO(String connectionString) {
		super(connectionString);
		// TODO Auto-generated constructor stub
	}
	
	public CareerDAO() {
		// TODO Auto-generated constructor stub
	}

	private static String table = "careerjet_jobs";
	
	
	public void insert(CareerData careerData){
		String sql = "INSERT INTO " + table + 
				"(date, country_code, title, location, description, "
				+ "company, salary_currency_code, "
				+ "salary, salary_min, salary_max, "
				+ "job_url) " +
				"VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		
		Connection conn = null;
		
		if (careerData.getJobs() == null)
		{
			return;
		}
		
		try {
			conn = DriverManager.getConnection(url);
			for (JetJob job : careerData.getJobs())
			{
				PreparedStatement ps = conn.prepareStatement(sql);
				ps.setDate(1, new java.sql.Date(job.getDate().getTime()));
				ps.setString(2, job.getCountryCode());
				ps.setString(3,  job.getTitle());
				ps.setString(4, job.getLocation());
				ps.setString(5,  job.getDescription());
				ps.setString(6, job.getCompany());
				ps.setString(7,  job.getSalaryCurrencyCode());
				ps.setObject(8, job.getSalary());
				ps.setObject(9, job.getSalaryMin());
				ps.setObject(10, job.getSalaryMax());
				ps.setString(11, job.getJobURL());
				ps.executeUpdate();
				ps.close();
			}
		} catch (SQLException e){
			throw new RuntimeException(e);
		} finally {
			if (conn != null){
				try {
					conn.close();
				} catch (SQLException e){
					
				}
			}
		}
	}
	
	private static Float GetNullableFloat(ResultSet rs, String key) throws SQLException {
		float value = rs.getFloat(key);
		if (value == 0.0F) {
			return null;
		}
		return Float.valueOf(value);
	}
	
	private JetJob JetJobFactory(ResultSet rs) throws SQLException {
		JetJob job  = new JetJob();
		job.setDate(rs.getDate("date"));
		job.setCountryCode(rs.getString("country_code"));
		job.setTitle(rs.getString("title"));
		job.setLocation(rs.getString("location"));
		job.setDescription(rs.getString("description"));
		job.setCompany(rs.getString("company"));
		job.setSalaryCurrencyCode(rs.getString("salary_currency_code"));
		job.setSalary(GetNullableFloat(rs, "salary"));
		job.setSalaryMin(GetNullableFloat(rs, "salary_min"));
		job.setSalaryMax(GetNullableFloat(rs, "salary_max"));
		job.setJobURL(rs.getString("job_url"));
		
		return job;
	}
	
//	public List<CareerData> findAllCareers() {
//		String sql = "SELECT * from " + table;
//		Connection conn = null;
//		
//		try {
//			conn = DriverManager.getConnection(url);
//			PreparedStatement ps = conn.prepareStatement(sql);
//			
//			ResultSet rs = ps.executeQuery();
//			List<CareerData> careers = new ArrayList<CareerData>();
//			while (rs.next()) {
//				careers.add(CareerDataFactory(rs));
//			}
//			rs.close();
//			ps.close();
//			return careers;
//		} catch (SQLException e){
//			throw new RuntimeException(e);
//		} finally {
//			if (conn != null) {
//				try {
//				conn.close();
//				} catch (SQLException e) {}
//			}
//		}
//	}
	
	public CareerData findByCountryCode(String countryCode){
		
		String sql = "SELECT TOP (20) * FROM " + table + " WHERE country_code = ? ORDER BY date";
		Connection conn = null;
		
		try {
			conn = DriverManager.getConnection(url);
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, countryCode);
			CareerData career = new CareerData();
			career.setCountryCode(countryCode);
			ResultSet rs = ps.executeQuery();
			List<JetJob> jobs = new ArrayList<JetJob>();
			while (rs.next()) {
				jobs.add(JetJobFactory(rs));
			}
			career.setJobs(jobs);
			rs.close();
			ps.close();
			return career;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			if (conn != null) {
				try {
				conn.close();
				} catch (SQLException e) {}
			}
		}
	}
}
