package webapp.api.career;

import java.util.Date;

public class JetJob {
	private Date date;
	private String countryCode;
	private String title;
	private String location;
	private String description;
	private String company;
	private String salaryCurrencyCode;
	private Float salary;
	private Float salaryMin;
	private Float salaryMax;
	private String url;
	

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSalaryCurrencyCode() {
		return salaryCurrencyCode;
	}

	public void setSalaryCurrencyCode(String salaryCurrencyCode) {
		this.salaryCurrencyCode = salaryCurrencyCode;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public Float getSalary() {
		return salary;
	}

	public void setSalary(Float salary) {
		this.salary = salary;
	}

	public Float getSalaryMin() {
		return salaryMin;
	}

	public void setSalaryMin(Float salaryMin) {
		this.salaryMin = salaryMin;
	}

	public Float getSalaryMax() {
		return salaryMax;
	}

	public void setSalaryMax(Float salaryMax) {
		this.salaryMax = salaryMax;
	}

	public String getJobURL() {
		return url;
	}

	public void setJobURL(String jobURL) {
		this.url = jobURL;
	}
	
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	
	@Override
	public String toString(){
		return "{" +
		    "date: " + date + 
			" title:" + title +
			" location:" + location +
			" description:" + description +
			" company:" + company + 
			" salaryCurrencyCode:" + salaryCurrencyCode +
			" salary:" + salary +
			" salaryMin:" + salaryMin +
			" salaryMax:" + salaryMax +
			" jobURL:" + url +
			"}";
	}
}
