package webapp.api.career;

import com.careerjet.webservice.api.*;
import java.util.*;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


import org.json.*;
import org.apache.*;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CareerData {
	private String countryCode;
	private List<JetJob> jobs;
	
	public String getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
		
		if (jobs != null) { 
			for (JetJob job : jobs) {
				job.setCountryCode(countryCode);
			}
		}
	}
	
	public List<JetJob> getJobs() {
		return jobs;
	}
	public void setJobs(List<JetJob> jobs) {
		this.jobs = jobs;
	}

	@Override
	public String toString(){ 
		String str = "{ ";
		if (jobs != null) { 
			for (JetJob job : jobs) {
				str += job.toString() + ", ";
			}
		}
		return str + "}";
	}

	public static String[] getHeaders() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public static String[] getValues() {
		return null;
	}
	
	
}
