package webapp;

import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import webapp.api.DataModel;
import webapp.api.DataModelWrapper;
import webapp.api.career.CareerDAO;
import webapp.api.career.CareerData;
import webapp.api.economy.EconomyDAO;
import webapp.api.economy.EconomyData;
import webapp.api.joshua.JoshuaCountry;
import webapp.api.joshua.JoshuaCountryDAO;

@RestController
public class ApiController {
	
	JoshuaCountryDAO countryDAO = new JoshuaCountryDAO();
	EconomyDAO ecoDAO = new EconomyDAO();
	CareerDAO careerDAO = new CareerDAO();
	
	@RequestMapping("/data")
	public DataModelWrapper data(){
		
		DataModelWrapper wrapper = new DataModelWrapper();
		List<JoshuaCountry> countryList;
		// EconomyData economyData = null; // TODO add support for economy data
		
		// source data from Joshua DAO
		// get all countries
		countryList = countryDAO.findAllCountries();
		
		// iterate through countries
		int count = 0;
		for (JoshuaCountry country : countryList) {
			String countryCode = country.getCountryCode();
			CareerData careerData = careerDAO.findByCountryCode(countryCode);
			DataModel dataModel = new DataModel();
			dataModel.setCountry(country);
			dataModel.setCareerData(careerData);
			wrapper.addDataModel(dataModel);
			count++;
			
			// default web page doesn't need more than 6.
			if (count > 8) {
				break;
			}
		}
		
		return wrapper;
		
	}
	
}
